-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05-Abr-2019 às 19:31
-- Versão do servidor: 10.1.21-MariaDB
-- versão do PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `casamento`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acompanhantes`
--

CREATE TABLE `acompanhantes` (
  `idacompanhante` int(10) UNSIGNED NOT NULL,
  `convidados_idConvidado` int(10) UNSIGNED NOT NULL,
  `acompanhante` int(11) NOT NULL,
  `convitesIndividuais` int(3) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `acompanhantes`
--

INSERT INTO `acompanhantes` (`idacompanhante`, `convidados_idConvidado`, `acompanhante`, `convitesIndividuais`) VALUES
(1, 1, 6, 4),
(2, 2, 5, 0),
(3, 3, 4, 0),
(4, 4, 2, 0),
(5, 5, 2, 0),
(6, 6, 2, 0),
(7, 7, 4, 0),
(8, 8, 2, 0),
(9, 9, 5, 0),
(10, 10, 1, 0),
(11, 11, 2, 0),
(12, 12, 2, 0),
(13, 13, 2, 0),
(14, 14, 4, 0),
(15, 15, 4, 0),
(16, 16, 4, 0),
(17, 17, 7, 0),
(18, 18, 4, 0),
(19, 19, 6, 0),
(20, 20, 5, 0),
(21, 21, 3, 0),
(22, 22, 2, 0),
(23, 23, 2, 0),
(24, 24, 2, 0),
(25, 25, 4, 0),
(26, 26, 2, 0),
(27, 27, 2, 0),
(28, 28, 2, 0),
(29, 29, 4, 0),
(30, 30, 2, 0),
(31, 31, 2, 0),
(32, 32, 2, 0),
(33, 33, 2, 0),
(34, 34, 2, 0),
(35, 35, 2, 0),
(36, 36, 2, 0),
(37, 37, 2, 0),
(38, 38, 2, 0),
(39, 39, 2, 0),
(40, 40, 2, 0),
(41, 41, 2, 0),
(42, 42, 2, 0),
(43, 43, 2, 0),
(44, 44, 2, 0),
(45, 45, 2, 0),
(46, 46, 3, 0),
(47, 47, 2, 0),
(48, 48, 4, 0),
(49, 49, 4, 0),
(50, 50, 2, 0),
(51, 51, 4, 0),
(52, 52, 2, 0),
(53, 53, 2, 0),
(54, 54, 2, 0),
(55, 55, 2, 0),
(56, 56, 6, 0),
(59, 59, 3, 0),
(60, 60, 2, 0),
(61, 61, 2, 0),
(62, 62, 1, 0),
(63, 63, 1, 0),
(64, 64, 3, 0),
(65, 65, 4, 0),
(66, 66, 4, 0),
(67, 67, 2, 0),
(68, 68, 3, 0),
(69, 69, 5, 0),
(70, 70, 2, 0),
(71, 71, 5, 0),
(72, 72, 3, 0),
(73, 73, 3, 0),
(74, 74, 5, 0),
(75, 75, 2, 0),
(76, 76, 2, 0),
(77, 77, 2, 0),
(78, 78, 2, 0),
(79, 79, 0, 0),
(80, 80, 0, 0),
(81, 81, 0, 0),
(82, 82, 0, 0),
(83, 83, 0, 0),
(84, 84, 0, 0),
(85, 85, 0, 0),
(86, 86, 0, 0),
(87, 87, 0, 0),
(88, 88, 0, 0),
(89, 89, 0, 0),
(90, 90, 0, 0),
(91, 91, 0, 0),
(92, 92, 0, 0),
(93, 93, 0, 0),
(94, 94, 0, 0),
(95, 95, 0, 0),
(96, 96, 0, 0),
(97, 97, 0, 0),
(98, 98, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `convidados`
--

CREATE TABLE `convidados` (
  `idConvidado` int(10) UNSIGNED NOT NULL,
  `nome` varchar(150) NOT NULL,
  `codigo` varchar(6) NOT NULL,
  `presenca` varchar(3) NOT NULL DEFAULT 'NAO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `convidados`
--

INSERT INTO `convidados` (`idConvidado`, `nome`, `codigo`, `presenca`) VALUES
(1, 'Joel e Familia', 'A1B2C3', 'SIM'),
(2, 'Levi e Familia', 'B8QC7J', 'NAO'),
(3, 'Josué e Família', '7S98L2', 'NAO'),
(4, 'Heromildo e Familia', 'K1Q1DO', 'NAO'),
(5, 'Pryscila e Leonardo', 'XBA0ND', 'NAO'),
(6, 'Mislane e Frederico', 'UJRM4K', 'NAO'),
(7, 'Delfina e Família', 'B6JR9H', 'NAO'),
(8, 'Pr. Abraão e Imaculada', '26025C', 'NAO'),
(9, 'Jônatas e Família', 'UG4B87', 'NAO'),
(10, 'Terezinha', 'QW9QLM', 'NAO'),
(11, 'Noemy e Gustavo', 'T7XCKQ', 'NAO'),
(12, 'Ismael e Elza', 'NLMX4L', 'NAO'),
(13, 'Afonso e Elvessiria', '4XIUPE', 'NAO'),
(14, 'Denise e família', 'XAKQCJ', 'NAO'),
(15, 'Rogério e família', 'OS5NAG', 'NAO'),
(16, 'Cláudio e família', '0UDNXN', 'NAO'),
(17, 'Marcio e família', '4A0GYT', 'NAO'),
(18, 'Luiz Carlos e família', 'SSH2DX', 'NAO'),
(19, 'Flávia e família ', '61XHHZ', 'NAO'),
(20, 'Rosangela e família', 'QXILAI', 'NAO'),
(21, 'Rosimeire e família', 'R14WEQ', 'NAO'),
(22, 'Ambrosina e Raimunda', 'A8R0FG', 'NAO'),
(23, 'Carla e Marcus', 'KZZRQ2', 'NAO'),
(24, 'Brendon', 'FMT2U1', 'NAO'),
(25, 'Neuma e família', 'RBO0NO', 'NAO'),
(26, 'Tamara', '95G3RL', 'NAO'),
(27, 'Caroline', 'KRK3IP', 'NAO'),
(28, 'Luciana Nishi', '9SFYBW', 'NAO'),
(29, 'Patrícia e família', 'QG9A9T', 'NAO'),
(30, 'Daniel Oliveira', '2QWGH9', 'NAO'),
(31, 'José Francisco', 'WH52BM', 'NAO'),
(32, 'Adson', 'DL5NFD', 'NAO'),
(33, 'Hugo', '4EXA2Y', 'NAO'),
(34, 'Nathalia', 'XJ5WJ4', 'NAO'),
(35, 'Melquisedeque', 'GCL3IS', 'NAO'),
(36, 'Ana Amâncio', '251YY2', 'NAO'),
(37, 'Adalberto', 'B1E6FJ', 'NAO'),
(38, 'Eduardo', 'AANYAG', 'NAO'),
(39, 'Jocélio', 'LXPHJ0', 'NAO'),
(40, 'Viviane e família', 'BQ2MJB', 'NAO'),
(41, 'Marcos Moraes', '6C6X0T', 'NAO'),
(42, 'Lucas Galvão', 'P1LTYR', 'NAO'),
(43, 'Paulo Vinicius', '3YSGLS', 'NAO'),
(44, 'Larissa', 'NYG35Q', 'NAO'),
(45, 'Mariana e Weber', 'S78LKH', 'NAO'),
(46, 'Elaine e família', 'MHKJ4A', 'NAO'),
(47, 'Rozania', '4J2F4P', 'NAO'),
(48, 'Hellen e família', 'YXEG4P', 'NAO'),
(49, 'Rosilene e família', '0HE7BA', 'NAO'),
(50, 'Edilene', 'NU8L6I', 'NAO'),
(51, 'Juliana e família', 'TJP4SK', 'NAO'),
(52, 'Lucas e família', '5NUXSO', 'NAO'),
(53, 'Estela', 'CHCHN6', 'NAO'),
(54, 'Thiago', 'YDE2AF', 'NAO'),
(55, 'Pedro e Thalita', '3O111X', 'NAO'),
(56, 'Cleusimar e Família', 'AL7QGQ', 'NAO'),
(59, 'Gabriel e Gabriela', '2B4MZM', 'NAO'),
(60, 'Aguimar e Edilamar', '22T498', 'NAO'),
(61, 'Suzane', '0XB5PC', 'NAO'),
(62, 'Maria Aparecida', 'BIR2AJ', 'NAO'),
(63, 'Delismar', 'PBUMR2', 'NAO'),
(64, 'Lauane e Família', '3K3XXS', 'NAO'),
(65, 'Lindemberg e Família', 'AOKUJK', 'NAO'),
(66, 'Degmar e Família', 'SA7U5M', 'NAO'),
(67, 'Mario e Irene', 'Y6WFZX', 'NAO'),
(68, 'Walter e Família', 'PEYAQG', 'NAO'),
(69, 'Macario e Família', '2KQW7E', 'NAO'),
(70, 'Allisson e Jackelliny', 'F8TPS3', 'NAO'),
(71, 'Sirlene e Família', '0C3YXZ', 'NAO'),
(72, 'Lindomar', 'BKUXQJ', 'NAO'),
(73, 'Francisco Artur e Família', 'K6N875', 'NAO'),
(74, 'Maurilio e Família', 'F0FXMD', 'NAO'),
(75, 'Thalita e Thiago', '1RBL7U', 'NAO'),
(76, 'Gustavo e Fernanda', 'EYX8LK', 'NAO'),
(77, 'Marcio Rogerio e Família', 'XMUIAC', 'NAO'),
(78, 'João Rubens e Família', 'RK86I6', 'NAO'),
(79, 'Jose Pedro Apolinário e Família', '2OW8B8', 'NAO'),
(80, 'Lunguinho Monteiro e Família', 'B3QDEN', 'NAO'),
(81, 'Paulo Monteiro e Família', 'YJCLHN', 'NAO'),
(82, 'Geraldo e Família', 'W41GC1', 'NAO'),
(83, 'Roberto e Família', 'JUBIRK', 'NAO'),
(84, 'Francisco e Família', 'EJ01H1', 'NAO'),
(85, 'Aline e Família', '0JTGMZ', 'NAO'),
(86, 'Antônia Gomes Pinheiro e Família', 'UA8XLF', 'NAO'),
(87, 'Vitor e família', 'TOZWXG', 'NAO'),
(88, 'Bruna Nascimento', '71QXRY', 'NAO'),
(89, 'David e Família', 'B8A2ED', 'NAO'),
(90, 'Kaue', 'OH79OK', 'NAO'),
(91, 'Sales e Família', 'Y9LWWY', 'NAO'),
(92, 'João Souza e Família', '2XW3P1', 'NAO'),
(93, 'Nádia e Família', '7AFWW2', 'NAO'),
(94, 'Jorge', '2STJGC', 'NAO'),
(95, 'Geny Saraiva', '7UJ5UY', 'NAO'),
(96, 'Wilame Saraiva', 'GIYR5J', 'NAO'),
(97, 'Genez Saraiva', 'FY72RN', 'NAO'),
(98, 'Patricia Marques', 'SONZA9', 'NAO');

-- --------------------------------------------------------

--
-- Estrutura da tabela `presentes`
--

CREATE TABLE `presentes` (
  `idPresente` int(10) UNSIGNED NOT NULL,
  `nome` varchar(150) NOT NULL,
  `descricao` varchar(300) NOT NULL,
  `quantidade` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `presentes`
--

INSERT INTO `presentes` (`idPresente`, `nome`, `descricao`, `quantidade`) VALUES
(1, 'Conjunto de Talheres', '', 1),
(2, 'Conjunto de Facas', '', 1),
(3, 'Jogo de Copos', '', 1),
(4, 'Jogo de Panelas', 'Antiaderente', 1),
(5, 'Panela de Pressão', '', 1),
(6, 'Jogo de Vasilhas', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acompanhantes`
--
ALTER TABLE `acompanhantes`
  ADD PRIMARY KEY (`idacompanhante`),
  ADD KEY `acompanhantes_FKIndex1` (`convidados_idConvidado`);

--
-- Indexes for table `convidados`
--
ALTER TABLE `convidados`
  ADD PRIMARY KEY (`idConvidado`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indexes for table `presentes`
--
ALTER TABLE `presentes`
  ADD PRIMARY KEY (`idPresente`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acompanhantes`
--
ALTER TABLE `acompanhantes`
  MODIFY `idacompanhante` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `convidados`
--
ALTER TABLE `convidados`
  MODIFY `idConvidado` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `presentes`
--
ALTER TABLE `presentes`
  MODIFY `idPresente` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `acompanhantes`
--
ALTER TABLE `acompanhantes`
  ADD CONSTRAINT `acompanhantes_ibfk_1` FOREIGN KEY (`convidados_idConvidado`) REFERENCES `convidados` (`idConvidado`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
