<footer>
<div class="verybottom">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="aligncenter">
					<p>
						 &copy;2015 Groovin - All right reserved | Designed by <a>BootstrapTaste</a>.
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
</footer>
<a href="#" class="scrollup"><i class="fa fa-angle-up fa-2x"></i></a>

<!-- estilo da tabela presentes  -->
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: center;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<!-- javascript -->
<script src="../../assets/js/jquery-1.9.1.min.js"></script>
<script src="../../assets/js/jquery.easing.js"></script>
<script src="../../assets/js/classie.js"></script>
<script src="../../assets/js/bootstrap.js"></script>
<script src="../../assets/js/slippry.min.js"></script>
<script src="../../assets/js/nagging-menu.js"></script>
<script src="../../assets/js/jquery.nav.js"></script>
<script src="../../assets/js/jquery.scrollTo.js"></script>
<script src="../../assets/js/jquery.fancybox.pack.js"></script>
<script src="../../assets/js/jquery.fancybox-media.js"></script>
<script src="../../assets/js/masonry.pkgd.min.js"></script>
<script src="../../assets/js/imagesloaded.js"></script>
<script src="../../assets/js/jquery.nicescroll.min.js"></script>
<script src="../../assets/js/validate.js"></script>
<script src="../../assets/js/AnimOnScroll.js"></script>
<script>
	new AnimOnScroll( document.getElementById( 'grid' ), {
	  minDuration : 0.4,
	  maxDuration : 0.7,
	  viewportFactor : 0.2
	} );
</script>
<script>
	//função de transição do slide
	$(document).ready(function(){
	  $('#slippry-slider').slippry(
		defaults = {
			transition: 'vertical',
			useCSS: true,
			speed: 5000,
			pause: 3000,
			initSingle: false,
			auto: true,
			preload: 'visible',
			pager: false,
		}
	  )
	});
</script>
</body>
</html>
