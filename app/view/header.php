<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Maycon e Laiza(03/08/2019) Página Inicial</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Casamento" />
<meta name="author" content="Maycon" />
<!-- styles -->
<link rel="stylesheet" href="../../assets/css/fancybox/jquery.fancybox.css">
<link href="../../assets/css/bootstrap.css" rel="stylesheet" />
<link href="../../assets/css/bootstrap-theme.css" rel="stylesheet" />
<link rel="stylesheet" href="../../assets/css/slippry.css">
<link href="../../assets/css/style.css" rel="stylesheet" />
<link rel="stylesheet" href="../../assets/color/default.css">
<script src="../../assets/js/modernizr.custom.js"></script>
</head>
<body>
<header>

<div id="navigation" class="navbar navbar-inverse navbar-fixed-top default" role="navigation">
  <div class="container">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">Laíza e Maycon</a>
    </div>
	<div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"><nav>
      <ul class="nav navbar-nav navbar-right">
        <li class="current"><a href="index.php#intro">Inicio</a></li>
    		<li><a href="index.php#sobre">Sobre</a></li>
    		<li><a href="index.php#albuns">Álbuns</a></li>
        <li><a href="index.php#presentes">Presentes</a></li>
    		<li><a href="index.php#informacao">Informações</a></li>
      </ul></nav>
    </div><!-- /.navbar-collapse -->
	</div>

  </div>
</div>

</header>
