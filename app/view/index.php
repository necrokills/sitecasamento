<?php
require_once('header.php');
require_once("../controller/PresenteController.php");
$presente = new PresenteController();
$presentes = $presente->getAll();
?>

<!-- section inicio -->
<section id="intro">
			<ul id="slippry-slider">
			  <li>
				<a href="#slide1"><img src="../../assets/img/slide/1.jpg"></a>
			  </li>
			  <li>
				<a href="#slide2"><img src="../../assets/img/slide/2.jpg" alt="BEM VINDOS AO NOSSO SITE!"></a>
			  </li>
			</ul>
</section>
<!-- end inicio -->
<!-- Section sobre -->
<section id="sobre" class="section">
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="heading">
        <?php
				//função contagem  regressiva
        $hoje = date("Y-m-d");
        $data = "2019-08-03";
        $dif = strtotime($data) - strtotime($hoje);
        $dias = ($dif / 86400);
        $dias = round ($dias, 0);
        ?>
				<h3><span>Faltam apenas <?php echo $dias ?> dias!</span></h3>
			</div>
		</div>
	</div>
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="team-box text-justify">
        <p class=" text-faded mb-4" style=" color: #000; font-weight: 400;">A contagem regressiva começa, o frio na barriga e toda a ansiedade do dia mais esperado de nossas vidas nos enche de alegria em tê-los ao nosso lado. Vamos juntos nesse grande sonho, o dia em que uniremos nossas almas e corpos para sempre, o dia do nosso casamento.</p>
      </div>
    </div>
  </div>
  <div class="sub-heading">
    <p>
      <button class="btn btn-lg btn-theme" onclick="location.href='confirmarPresenca.php'">Confirmar Presença</button>
    </p>
  </div>
</div>
</section>
<!-- end section sobre -->
<!-- section albuns -->
<section id="albuns" class="section gray">
<div class="container">
	<div class="row">
	</div>
	<div class="row">
		<div class="col-md-12">
						<ul class="grid effect" id="grid">
						<li><a href="../../assets/img/albuns/1.jpg" class="fancybox" data-fancybox-group="gallery" title="foto name"><img src="../../assets/img/albuns/1.jpg" alt="" /></a></li>
						<li><a href="../../assets/img/albuns/2.jpg" class="fancybox" data-fancybox-group="gallery" title="foto name"><img src="../../assets/img/albuns/2.jpg" alt="" /></a></li>
						<li><a href="../../assets/img/albuns/3.jpg" class="fancybox" data-fancybox-group="gallery" title="foto name"><img src="../../assets/img/albuns/3.jpg" alt="" /></a></li>
						<li><a href="../../assets/img/albuns/4.jpg" class="fancybox" data-fancybox-group="gallery" title="foto name"><img src="../../assets/img/albuns/4.jpg" alt="" /></a></li>
						<li><a href="../../assets/img/albuns/5.jpg" class="fancybox" data-fancybox-group="gallery" title="foto name"><img src="../../assets/img/albuns/5.jpg" alt="" /></a></li>
						<li><a href="../../assets/img/albuns/6.jpg" class="fancybox" data-fancybox-group="gallery" title="foto name"><img src="../../assets/img/albuns/6.jpg" alt="" /></a></li>
						<li><a href="../../assets/img/albuns/7.jpg" class="fancybox" data-fancybox-group="gallery" title="foto name"><img src="../../assets/img/albuns/7.jpg" alt="" /></a></li>
						<li><a href="../../assets/img/albuns/8.jpg" class="fancybox" data-fancybox-group="gallery" title="foto name"><img src="../../assets/img/albuns/8.jpg" alt="" /></a></li>
						<li><a href="../../assets/img/albuns/9.jpg" class="fancybox" data-fancybox-group="gallery" title="foto name"><img src="../../assets/img/albuns/9.jpg" alt="" /></a></li>
						</ul>
		</div>
	</div>
</div>
</section>
<!-- section albuns -->
<!-- section presentes -->

<section id="presentes" class="section">
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="heading">
				<h3><span>Lista de Presentes</span></h3>
			</div>
		</div>
    <div class="col-md-8 col-md-offset-2">
      <div class="team-box text-justify">
        <p class=" text-faded mb-4" style=" color: #000; font-weight: 400;">Ajude-nos a montar o nosso novo lar! Aqui vocês poderão encontrar nossa lista de presentes.</p>
			</div>
    </div>
  </div>

	<div class="container-fluid">
		<div class="table-responsive">
			<p class=" text-faded mb-4" style=" color: #000; font-weight: 400;">A lista de presente possui um controle de quantidade de cada item na tabela. <br>Cujo intuito é mostrar a todos, os presentes que ainda não foram confirmados.</p>
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <tr>
	        <th>Nome</th>
	        <th>Descrição</th>
	        <th>Posso Presentear</th>
					<th>Quantidade</th>
        </tr>
				<?php
					foreach($presentes as $key => $presente) {
						echo "<tr>";
							echo "<td>".$presente["nome"]."</td>";
							echo "<td>".$presente["descricao"]."</td>";
							echo "<td>
								<a onclick='presentear(".$presente['idPresente'].", ".$presente['quantidade'].");'>
								<span class='fa fa-gift'></span></a>
										</td>";
							echo "<td>".$presente["quantidade"]."</td>";
						echo "</tr>";
					}
				?>
			</table>
		</div>
	</div>
</div>
</section>

<section id="informacao" class="section">
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="heading">
				<h3><span>INFORMAÇÕES</span></h3>
			</div>
			<div class="sub-heading">
				<p>
					 Informações importantes
				</p>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-6">
				<h4>Local do Casório</h4>
				<p>Alfa Place Eventos</p>
        <p>Avenida N2, Quadra 20, lote 19 e 20 - Anápolis City, Anápolis - GO, 75094-060</p>
				<!-- map -->
				<div class="clearfix col-lg-12 ml-auto text-center">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d957.2718363250356!2d-48.93331877083956!3d-16.318480699295463!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935ea444fddfaf0d%3A0x422d19150520752a!2sAlfa+Place+Eventos!5e0!3m2!1spt-BR!2sbr!4v1549074645355" width="300" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
		</div>
	</div>
</div>
</section>
<!-- end section informacao -->

<?php
require_once('footer.php');
?>

<script type="text/javascript">
//função buscar lista e atualizar informações no banco, se sucesso atualiza a pagina.
function presentear(idPresente, quantidade)
{
  if (confirm("Deseja nos presentear com este presente?")) {

	$.ajax({
		type: "POST",
		url: "../route/editar.php",
		data: {
			'acao': 'addPresente',
			'presente_id': idPresente,
			'quantidade': quantidade,
		},
		success: function(resp) {
			location.reload();
		},
		dataType: 'json'
	});
  } else {
  }
}
</script>
