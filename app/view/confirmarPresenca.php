<?php
require_once('header.php');
require_once("../controller/PresenteController.php");

$presente = new PresenteController();
$presentes = $presente->getAll();
?>
<!-- Inicio da seleção confirmação de presença -->
<section id="selectionPresenca" class="section">
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="heading">
				<h3><span>Confirmação de Presença</span></h3>
			</div>
			<div class="sub-heading">
				<p>Faça parte da nossa história de amor, confirme sua presença.</p>
			</div>
			<div class=" sub-heading">
				<p class="text-left"> 1º Informe o <b>código</b> igual ao convite.</p>
				<p class="text-left">	2º Clique em <b>continuar</b>.</p>
				<p class="text-left">	3º Confirme sua <b>presença</b> e a <b>quantidade de convites individuais</b>.</p>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12 ">
			<h4><i class="icon-envelope"></i><strong>Dados do convidado</strong></h4>
			<div class="cform" id="buscarConvidadoForm" style="display: true;">
				<form id="contact-form"	>
				  <div class="form-group">
					<label for="codigo">Código do convite:</label>
					<input type="text" class="form-control" maxlength="6" name="codigo" id="codigo" required="" placeholder="Código" data-rule="codigo" onkeyup="this.value = this.value.toUpperCase();"/>
					<div class="validation"></div>
				  </div>
					<input type="hidden" id="inputHidden" />
				  <button type="button" id="buscar" name="buscar"  class="btn btn-lg btn-theme pull-left">Continuar</button>
				</form>
			</div>
			<div class="clear"></div>

			<div class="cform" id="confirmarPresencaForm" style="display: none">
				<div id="contact-form">
					<form id="atualizarConvidado">
						<div class="form-group">
							<label for="nome">Nome do convite:</label>
							<input type="text" name="nome" class="form-control" id="nome" disabled="disabled"/>
							<div class="validation"></div>
						</div>
						<div class="form-group">
						<label for="nome">Você irá ao evento?</label>
						<select type="text" class="form-control" name="presenca" id="presenca">
							<option value="SIM">Sim</option>
							<option value="NAO">Não</option>
						</select>
						<div class="validation"></div>
						</div>
						<div class="form-group">
							<label for="codigo">Quantidade total de convites individuais que serão usados?</label>
							<select type="text" class="form-control" name="convidados" id="convidados">
							</select>
							<div class="validation"></div>
						</div>
						<button type="button" id="confirmarPresenca" class="btn btn-lg btn-theme pull-left" onclick="location.href='confirmarPresenca.php'">Confirmar</button>
						<input type="hidden" name="acao" value="atualizarConvidado">
					</form>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
</section>
<!-- Fim da seleção confirmação de presença -->

<footer>
<div class="verybottom">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="aligncenter">
					<p>&copy;2015 Groovin - All right reserved | Designed by <a>BootstrapTaste</a>.</p>
				</div>
			</div>
		</div>
	</div>
</div>
</footer>
<a href="#" class="scrollup"><i class="fa fa-angle-up fa-2x"></i></a>

<!-- javascript -->
<script src="../../assets/js/jquery-1.9.1.min.js"></script>
<script src="../../assets/js/jquery.easing.js"></script>
<script src="../../assets/js/classie.js"></script>
<script src="../../assets/js/bootstrap.js"></script>
<script src="../../assets/js/slippry.min.js"></script>
<script src="../../assets/js/nagging-menu.js"></script>
<script src="../../assets/js/jquery.nav.js"></script>
<script src="../../assets/js/jquery.scrollTo.js"></script>
<script src="../../assets/js/jquery.fancybox.pack.js"></script>
<script src="../../assets/js/jquery.fancybox-media.js"></script>
<script src="../../assets/js/masonry.pkgd.min.js"></script>
<script src="../../assets/js/imagesloaded.js"></script>
<script src="../../assets/js/jquery.nicescroll.min.js"></script>
<script src="../../assets/js/validate.js"></script>
<script src="../../assets/js/AnimOnScroll.js"></script>

<script type="text/javascript">
/*Função para buscar dados de acordo com o parametro "codigo"*/
	$('#buscar').click(function(){
		$.ajax({
	    type: "POST",
	    url: "../route/listar.php",
	    data: {
	      'acao': 'buscarConvidado',
	      'codigo': $('#codigo').val()
	    },
	    success: function(convidado) {
				if (convidado == false) {
					console.log(convidado);
					alert("Código Inválido! Digite novamente!");
					location.reload();
				}else {
					/*Função para esconder/mostrar divs*/
					$('#buscarConvidadoForm').css('display','none');
					$('#confirmarPresencaForm').show();
					/* Fim função esconder/mostrar divs*/
		      $("#nome").val(convidado.nome);
		      $("#presenca").val(convidado.presenca);
					listaConvidados = '';
					convidado.acompanhante.forEach(function(conv, i) {
						var quantidadeMaxima = conv; var x =1;	array=[];
						while (x <= quantidadeMaxima) {
							array[x] = x;
							listaConvidados += '<option value='+array[x]+'>'+array[x]+'</option>';
							x++;
						}
					});
					$('#convidados').html(listaConvidados);
					$("#confirmarPresenca").attr("onclick", "atualizar("+convidado.idConvidado+")");
				}
	    },
	    dataType: 'json'
	  });
	})
	function atualizar(idConvidado){
		var formDados = $("#atualizarConvidado").serialize();
		formDados += "&idConvidado="+idConvidado;
		$.ajax({
			url: "../route/editar.php",
			type: "POST",
			dataType: "json",
			data: formDados,
			success: function(resp) {
				alert(resp);
				location.reload();
			},
			dataType: 'json'
		});
	}
</script>
</body>
</html>
