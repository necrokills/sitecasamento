<?php
require_once("Database.php");

class Presentes {

  private $pdo;

  #construtor conexão
  public function __construct()
  {
      $this->pdo = new Database();
      $this->pdo = $this->pdo->getConexao();
  }

  #busca todos os presentes cadastrados
  public function getAll()
  {
    $stmt = $this->pdo->prepare("SELECT * FROM presentes");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }
  #Atualiza a linha quantidade da tabela presentes de acordo com o ID
  public function adicionarPresente($id, $quantidade)
  {
    $stmt = $this->pdo->prepare('UPDATE presentes SET quantidade = :quantidade WHERE idPresente = :id');
    $stmt->execute(array(
    ':id'   => $id,
    ':quantidade' => $quantidade
    ));
    return $stmt->rowCount();
  }
}
