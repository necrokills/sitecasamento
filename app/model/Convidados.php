<?php
require_once("Database.php");

class Convidados {

  private $pdo;

  #construtor conexão
  public function __construct()
  {
      $this->pdo = new Database();
      $this->pdo = $this->pdo->getConexao();
  }

  #busca codigo do convidado especifico e retorna linhas da tabela Convidados e Acompanhantes
  public function buscarConvidado($codigo)
  {
    $stmt = $this->pdo->prepare("SELECT
                                  CONV.idConvidado,
                                  CONV.nome,
                                  CONV.codigo,
                                  CONV.presenca,
                                  ACOMP.idacompanhante as idacompanhante
                                 FROM
                                  acompanhantes AS ACOMP
                                 INNER JOIN
                                  convidados AS CONV on (CONV.idConvidado = ACOMP.convidados_idConvidado)
                                 WHERE
                                  codigo = '$codigo'");
    $stmt->execute();
    if($stmt->rowCount() > 0){
      return $stmt->fetch(PDO::FETCH_ASSOC);
    }
      return false;
  }

  public function buscarPenetra($codigo)
  {
    $stmt = $this->pdo->prepare("SELECT
                                  ACOMP.acompanhante as acompanhante
                                 FROM
                                  acompanhantes AS ACOMP
                                 INNER JOIN
                                  convidados AS CONV on (CONV.idConvidado = ACOMP.convidados_idConvidado)
                                 WHERE
                                  codigo = '$codigo'");
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $var;
    foreach ($result as $key => $value) {
      $var[] = $value['acompanhante'];
    }
    return $var;
  }

  public function atualizar($idConvidado,$presenca, $convitesIndividuais)
  {
    $stmt = $this->pdo->prepare('UPDATE
                                  convidados
                                 INNER JOIN
                                  acompanhantes on convidados.idConvidado = acompanhantes.convidados_idConvidado
                        				 SET
                        				  presenca = :presenca, convitesIndividuais = :convitesIndividuais
                                 WHERE
                                   idConvidado = :idConvidado');
    $stmt->execute(array(
    ':idConvidado'   => $idConvidado,
    ':presenca' => $presenca,
    ':convitesIndividuais' =>$convitesIndividuais
    ));
    return $stmt->rowCount();
  }

}



// public function buscarConvidado($codigo)
// {
//   $stmt = $this->pdo->prepare("SELECT
//                                 CONV.idConvidado,
//                                 CONV.nome,
//                                 CONV.codigo,
//                                 CONV.presenca,
//                                 ACOMP.idacompanhante as idacompanhante,
//                                 ACOMP.acompanhante as acompanhante
//                                FROM
//                                 acompanhantes AS ACOMP
//                                INNER JOIN
//                                 convidados AS CONV on (CONV.idConvidado = ACOMP.convidados_idConvidado)
//                                WHERE
//                                 codigo = '$codigo'");
//   $stmt->execute();
//   return $stmt->fetch(PDO::FETCH_ASSOC);
// }
