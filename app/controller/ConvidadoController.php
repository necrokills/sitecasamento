<?php

require_once('../model/Convidados.php');

class ConvidadoController {

  #Busca o convidado de acordo com o codigo informado
  public function buscarConvidado() {
      $convidado = new Convidados();
      $codigo = $_POST['codigo'];
      $infoConvidado = $convidado->buscarConvidado($codigo);
      if ($infoConvidado == false) {
        return false;
      }
      $penetra['acompanhante'] = $convidado->buscarPenetra($codigo);
      $result = array_merge($infoConvidado, $penetra);
      return $result;
  }

  #Atualiza dados do convidado de acordo com o ID
  #Faltando terminar
  public function atualizar() {
    $convidado = new Convidados();
    $idConvidado = $_POST['idConvidado'];
    $presenca = $_POST['presenca'];
    $convitesIndividuais = $_POST['convidados'];
    if ($convidado->atualizar($idConvidado, $presenca, $convitesIndividuais)) {
      return "Sua presença foi confirmada com sucesso!";
    }
      return "Não foi possivel confirmar sua presença!";
  }

}
