<?php

require_once("../model/Presentes.php");

class PresenteController {

  #busca todos os presentes cadastrados no site
  public function getAll() {
      $presente = new Presentes();
      return $presente->getAll();
  }
  #Atualiza a quantidade do presente de acordo com o id
  public function adicionarPresente() {
      $presente = new Presentes();
      $id = $_POST['presente_id'];
      $quantidade = $_POST['quantidade'];
      $quantidade = $quantidade+1;
      return $presente->adicionarPresente($id, $quantidade);
  }

}
